from PyPDF2 import PdfFileWriter, PdfFileReader

inputpdf = PdfFileReader(open("main.pdf", "rb"))

examnum = '3'
forms = 3
pages = 6
anspages = 1
for form in range(0,forms * pages,pages):
    output = PdfFileWriter()
    for page in range(pages-anspages):
        output.addPage(inputpdf.getPage(form+page))

    letters = ["A","B","C","D","E"]
    letter = letters[int(form/pages)]
    with open("Exam" + examnum + "Form%s.pdf" % letter, "wb") as outputStream:
        output.write(outputStream)

output = PdfFileWriter()
for form in range(0,forms * pages, pages):
    for page in range(pages-anspages,pages):
        output.addPage(inputpdf.getPage(form+page))
        
with open("AnsKeyExam%s.pdf" % examnum, "wb") as outputStream:
    output.write(outputStream)
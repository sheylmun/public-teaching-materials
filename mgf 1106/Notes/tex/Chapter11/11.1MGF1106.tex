\begin{frame}[shrink]{Objectives}
	\begin{enumerate}
		\item Understand the history and nature of probability
		\item Understand empirical probability
		\item Understand the law of large numbers
		\item Understand theoretical probability
	\end{enumerate}
\end{frame}
\begin{frame}[shrink]{Important Mathemetician}
	\begin{rmk}
		\emph{Jacob Bernoulli} (1654-1706) was one the more prolific members of the Bernoulli family of Swiss mathemeticians.  He was a student of Gottfried Leibniz and the two collaborated to develop and advance the theory of Calculus that we use today. His most important contributions, however, were to the field of probability theory.  He was the first to propose what we know as the ``law of large numbers.''
	\end{rmk}
\end{frame}
\begin{frame}[shrink]{Probability Terms}
	\begin{deff}
		\begin{itemize}
			\item An \emph{experiment} is any controlled operation that yields a set of results.\pause{}
			\item The possible results of an experiment are called its \emph{outcomes}.\pause{}
			\item The set of all possible outcomes is called the \emph{sample space}.\pause{}
			\item Any single element of the sample space is called a \emph{sample point}.\pause{}
			\item An \emph{event} is a subset of the sample space of an experiment, denoted by \emph{$E$}.\pause{}
		\end{itemize}
	\end{deff}
\end{frame}

\begin{frame}[shrink]{Empirical Probability}
	\begin{deff}
		The \emph{Empirical Probability} of an event, $P(E)$, is found as follows
		$$P(E) =\dfrac{\text{number of times } E \text{ occurs}}{\text{number of times the experiment is performed}}$$
	\end{deff}\pause{}
	
	\begin{rmk}
		Empirical probability applies to situations where we can observe how frequently an event occurs.  Empirical probabilities are used extensively in actuarial sciences. For example, car insurance companies use empirical probabilities to determine the chance of a person being in a car accident based on historical data about car accidents in their demographics (location, type of car, age, gender, race, etc.).
	\end{rmk}
\end{frame}

\begin{frame}[shrink]{Empirical Probability}
	\begin{exmp}
		Census data from 2010:
		\begin{itemize}
			\item Total Population: $18,801,310$
			\begin{itemize}
				\item Males: $9,189,355	$
				\begin{itemize}
					\item Under 21 years of age: $2,440,481$
					\item 21-years of age or older: $6,748,874$
				\end{itemize}
				\item Females: $9,611,955$
				\begin{itemize}
					\item Under 21 years of age: $2,330,539$
					\item 21-years of age or older: $7,281,416$
				\end{itemize}
			\end{itemize}
		\end{itemize}
		If a person was randomly selected from the population of Florida at the time of the census, what is the probability that they are
		\begin{enumerate}
			\item Female?
			\item Male under 21 years of age?
			\item Over 21 years of age?
		\end{enumerate}
	\end{exmp}
\end{frame}

\begin{frame}[shrink]{The Law of Large Numbers}
	\begin{exmp}
		Suppose you flip the coin $10$ times, and you get heads on $4$ of the flips.  What is the empirical probability of heads?\pause{}
		
		If you flip the coin $100$ times, and observe heads $43$ times, what is the empirical probability of heads?
	\end{exmp}\pause{}
	\begin{rmk}
		The \emph{law of large numbers} states the following:
		
		Probability statements apply in practice to a large number of trials, not a single trial.  It is the relative frequency over the long run that is accuratelyy predictable, not individual events or precise totals.
	\end{rmk}
\end{frame}
\begin{frame}[shrink]{The Law of Large Numbers}
	\begin{exmp}
		Below is a table showing the results of flipping a fair coin many times at several stages:
		\begin{tabular}{cccc}
			\emph{Number}&\emph{Expected}&\emph{Observed}&\emph{Frequency}\\
			\emph{of}&\emph{number}&\emph{number}&\emph{of}\\
			\emph{tosses}&\emph{of heads}&\emph{of heads}&\emph{heads}\\
			10 & 5 & 4 & 0.4\\
			100 & 50 & 45 & 0.45\\
			1000 & 500 & 546 & 0.546\\
			10,000 & 5000 & 4852 & 0.4852\\
			100,000 & 50,000 & 49.770 & 0.49770
		\end{tabular}\pause{}
		
		
		We see the trend that as the number of experiments gets larger, the empirical probability of heads gets closer and closer to $0.5$.
	\end{exmp}
\end{frame}
\begin{frame}[shrink]{Theoretical Probability}
	\begin{deff}
		\begin{itemize}
			\item If each outcome of an experiment has the same chance of occurring, we say the outcomes are \emph{Equally Likely Outcomes}.\pause{}
			\item If an event $E$ has equally likely outcomes, the \emph{theoretical probability} of event $E$ is
			$$P(E) = \dfrac{\text{number of outcomes favorable to } E}{\text{total number of possible outcomes}}$$
		\end{itemize}
	\end{deff}
\end{frame}
\begin{frame}
	\begin{exmp}
		In the case of flipping a fair coin, there are two possible outcomes, $\{Heads, Tails\}$.  Some possible events: 
		\begin{itemize}
			\item Flipping the coin and getting Heads
			\item Flipping the coin and getting Tails
		\end{itemize}\pause{}
		The probability of flipping heads is $1/2$.
	\end{exmp}\pause{}
	\begin{exmp}
		A six-sided die is rolled once.  Find the probability of rolling 
		\begin{enumerate}
			\item a $3$\pause{}: Probability $=1/6$
			\item an even number\pause{}: Probability $=3/6$\pause{}$=1/2$
			\item a $5$ or a $6$\pause{}: Probability $=2/6$\pause{}$=1/3$
		\end{enumerate}
	\end{exmp}
\end{frame}
\begin{frame}[shrink]{Important Facts of Probability}
	\begin{itemize}
		\item The probability of an event that cannot occur is $0$.
		\item The probability of an event that must occur is $1$.
		\item Every probability will be between $0$ and $1$ inclusive.
		\item The sum of the probabilities of all possible mutually exclusive outcomes of an experiment is $1$.
		\item The sum of the probability of an even $A$ occuring, $P(A)$, and the probability of $A$ not occurring, $P(\text{not }A)$, is $1$:
		$$p(A) + P(\text{not }A) = 1$$
		A corrolary of this is that $P(\text{not }A) = 1 - P(A)$.
	\end{itemize}
\end{frame}
\begin{frame}
	\begin{exmp}
		You are dealt one card from a standard $52$-card deck.  What is the probability that your card is
		\begin{enumerate}
			\item an ace of diamonds?
			\item a face card?
			\item a club?
			\item a heart and a spade?
			\item not an $8$?
			\item a card greater than $5$ and less than $9$?
		\end{enumerate}
	\end{exmp}
\end{frame}




%\begin{frame}[shrink]{Objectives}
%	\begin{itemize}
%		\item Use the Fundamental Counting Principle to determine the number of possible outcomes of a given situation
%	\end{itemize}
%\end{frame}
%\begin{frame}[shrink]
%	\begin{deff}
%		If you can choose one item from a group of $M$ items and a second item from a group of $N$ items, then the total number of two-item choice options can be found to be $M\cdot N$
%	\end{deff}\pause{}
%	\begin{exmp}
%		If you own three pairs of shoes, and you have two different colors of socks, how may footwear-combination options do you have?
%	\end{exmp}\pause{}
%	\begin{deff}
%		A \emph{tree diagram} is one possible representation of all the choices you have.
%	\end{deff}
%\end{frame}
%\begin{frame}
%	\begin{exmp}
%		A food truck serves meals consisting of a main course and a side item.  If they have 8 different main dishes, and 5 different sides, how many meal options are there to choose from?
%	\end{exmp}\pause{}
%	\begin{exmp}
%		Suppose a meal also comes with a drink, and they have 4 drink options.  How many different meals can you order?
%	\end{exmp}
%\end{frame}
%\begin{frame}
%	\begin{deff}
%		The number of ways in which a series of successive things can occur is found by multiplying the number of ways in which each thing can occur.
%	\end{deff}\pause{}
%  	\begin{exmp}
%		If you own eight pairs of pants, three pairs of shoes, and two different colors of sock, how may different ways can you clothe your lower body?
%	\end{exmp}
%\end{frame}
%\begin{frame}
%	\begin{exmp}
%		You are planning your schedule for the next semester. There are 6 sections of the math class you need, 4 of the English class, and 5 of a satisfactory humanities class.  Assuming none of the different types conflict with each other (perhaps all of the maths are at the same time, all of the English classes are at the same time, and all of the humanities are at the same time), how many different course schedules could you have?
%	\end{exmp}
%\end{frame}
%\begin{frame}
%	\begin{exmp}
%		You are taking a multiple choice test consisting of ten questions.  Each has four answer choices with only one correct answer.  If you select one choice for each question, leaving nothing blank, in how many ways could you fill out the scantron?
%	\end{exmp}
%\end{frame}
%\begin{frame}
%	\begin{exmp}
%		U.S. telephone numbers begin with three- digit area codes followed by seven-digit local telephone numbers.  Area codes and local telephone numbers cannot begin with 0 or 1.  How many different telephone numbers are possible?
%	\end{exmp}
%\end{frame}
%\begin{frame}
%	\begin{exmp}
%		How many ways can the letters of the alphabet be rearranged?\pause{}
%		
%		$$26\cdot 25\cdot 23\cdot 22\cdot... \cdot 3\cdot 2 \cdot 1\pause{} = 403,291,461,126,605,635,584,000,000$$
%	\end{exmp}
%	\pause{}
%
%\end{frame}
\begin{frame}[shrink]{Objectives}
	\begin{enumerate}
		\item Understand logic in the English language
		\item Identify statements and logical connectives
		\item Understand quantifiers and identify the negations of statements containing quantifiers
		\item Identify compound statements.
		\item Identify statements using the words \textit{not},  \textit{and}, \textit{or}, \textit{if-then}, and \textit{if and only if}
	\end{enumerate}
\end{frame}
\begin{frame}{Names of Note}
	\begin{itemize}
		\item Aristotle
		\item Lewis Carroll (Charles Lutwidge Dodgson)
		\item George Boole
		\item Augustus De Morgan
	\end{itemize}
\end{frame}
\begin{frame}[shrink]
		\begin{deff}
			A \emph{statement} is a \textit{declarative} sentence that is either true or false, but not both simultaneously.  Labeling a statement true or false is called \emph{assigning a truth value} to the statement.
		\end{deff}\pause{}
		\begin{exmp}
			Some statments:
			\begin{enumerate}
				\item The Last Supper was painted by Leonardo da Vinci. \pause{} \textit{True} \pause{}
				\item J.K. Rowling wrote the Declaration of Independence. \pause{} \textit{False}
			\end{enumerate}
		\end{exmp}
	\end{frame}
\begin{frame}[shrink]
	\begin{rmk}
		Commands, questions, and opinions, are \emph{not}  statements
	\end{rmk}\pause{}
	\begin{exmp}
		\begin{enumerate}
			\item \textit{The Dark Knight} is the greatest movie of all time.
			\item Complete exercises $1$ - $7$ by Tuesday.
			\item In which room will the review be held?
		\end{enumerate}
	\end{exmp}
\end{frame}
\begin{frame}[shrink]
	\begin{rmk}
		In formal logic, lowercase letters such as $p, q, r,$ and $s$ are used to represent entire statements.
	\end{rmk}
	\begin{exmp}
		\begin{enumerate}	
			\item $p$: Sofia is the capital city of Bulgaria.
			\item $q$: Alexandre Dumas wrote \textit{A Christmas Carol}.
		\end{enumerate}
	\end{exmp}
\end{frame}

\begin{frame}[shrink]
	\begin{deff}
		\emph{Quantifiers:} are the words \emph{all, some,} and \emph{no} (or \emph{none}).
	\end{deff}
	\begin{exmp}
		Some statements containing quantifiers:
		\begin{enumerate}
			\item Some people are lactose-intolerant.
			\item No even numbers are negative.
			\item All poets are writers.
			\item Some statements are questions.
		\end{enumerate}
	\end{exmp}
\end{frame}

\begin{frame}[shrink]
	\begin{deff}
		The \emph{negation of a statement} is another statement with opposite meaning of the original.  \pause{}\\
		The negation of a true statement will be a false statement, and the negation of a false statement will be a true statement.
	\end{deff}\pause{}
	\begin{exmp}
		\begin{enumerate}
			\item Leonardo da Vinci painted \textit{The Last Supper}.\pause{}
			
			Negation: Leonardo da Vinci did not paint \textit{The Last Supper}.\pause{}
			\item Today is not Wednesday.\pause{}
			
			Negation: Today is Wednesday.
		\end{enumerate}
	\end{exmp}
\end{frame}

\begin{frame}[shrink]{Negation}
	The quantifier ``All are" is negated as ``Some are not'' (and vice versa).\pause{}
	
	The quantifier ``None are" is negated as ``Some are"  (and vice versa).
	\begin{exmp}
		The mechanic told me, ``\textit{all valves were replaced}," but I later learned that the mechanic never tells the truth.  What can I infer?\pause{}
		
		``\textit{Some} valves \textit{were not} replaced."
	\end{exmp}
	\begin{exmp}
		I do not believe my fried who said that ``\textit{some stars are made of chocolate pudding}".  The negation of this statement would be...\pause{}
		
		``\textit{No} stars \textit{are} made of chocolate pudding."
	\end{exmp}
\end{frame}

\begin{frame}[shrink]{Not Statements (Negation)}
	\begin{deff}
		The symbol $\sim$ preceding a statement is how we represent the negation of that statement.
	\end{deff}
	\begin{exmp}
		Let $p$ and $q$ represent the following statements:
		\begin{enumerate}
			\item $p$: Shakespeare wrote the last episode of \textit{Seinfeld}.
			\item $q$: Today is not Wednesday.
			\pause{}
		\end{enumerate}
		We can represent the following negations symbolically: \pause{}
		\begin{enumerate}
			\item ``Shakespeare did not write the last episode of \textit{Seinfeld}." \pause{}as $\sim p$ \pause{} and
			\item ``Today is Wednesday."\pause{} as $\sim q$.
		\end{enumerate}
	\end{exmp}
\end{frame}

\begin{frame}[shrink]{Compound Statements}
	\begin{deff}
		\begin{enumerate}
			\item Connectives are words used to join simple statements.  For instance, \emph{and, or, if... then,} and \emph{if and only if}.
			
			\item \emph{Simple statements} convey one idea with no connectives.
			
			\item \emph{Compound statements} combine two or more simple statements using connectives.
			
		\end{enumerate}
	\end{deff}
\end{frame}

\begin{frame}[shrink]{\emph{And} Statements}
	\begin{deff}
		If $p$ and $q$ are two simple statements, then the \emph{conjunction}, the compound statement \emph{``$p$ and $q$"}, is symbolized by $p\wedge q$.
	\end{deff}
\end{frame}

\begin{frame}[shrink]
	\begin{exmp}
		Let $p$ represent the simple statement ``It is raining." and $q$ represent the following simple statement ``They are going golfing."\pause{}  Write the following compound statements symbolically:
		\begin{enumerate}
			\item It is raining and they are going golfing.\pause{}
			$p\wedge q$\pause{}
			
			\item It is \textit{not} raining and they are going golfing.\pause{}
			$\sim p\wedge q$\pause{}
			
			
			\item It is raining and they are \textit{not} going golfing.\pause{}
			$p\wedge \sim q$\pause{}
		\end{enumerate}
	\end{exmp}
	\begin{rmk}
		Other words can be associated with the use of $\wedge$, such as \emph{but, yet,} and \emph{nevertheless}
	\end{rmk}
\end{frame}


\begin{frame}[shrink]
	\begin{exmp}
		In English, connectives can have dual meaning.  For instance, ``I ate fish or chicken." could mean either of the following: 
		\begin{enumerate}
			\item ``I ate fish or chicken but not both." (\emph{exclusive `or'} - one or the other exclusively)
			\item ``I ate fish or chicken or both." (\emph{inclusive `or'})
		\end{enumerate}
	\end{exmp}
\end{frame}
\begin{frame}[shrink]{Or Statements}
	\begin{deff}
		A \emph{disjunction} is a compound statement formed using the \emph{inclusive or}, symbolized by $\vee$.  Thus, ``$p$ or $q$ or both" is symbolized by $p\vee q$.
	\end{deff}\pause{}
	\begin{exmp}
		Let $p$ be the statement ``The bill will receive majority approval." and $q$ be the statement ``The bill will become law."\pause{}
		
		Write each compound statement symbolically:
		\begin{enumerate}
			\item The bill will receive majority approval, or the bill will become law.\pause{} $p\vee q$\pause{}
			\item The bill will receive majority approval, or the bill will not become law.\pause{} $p\vee \sim q$				
		\end{enumerate}
	\end{exmp}
\end{frame}

\begin{frame}[shrink]{Interpreting Commas}
	\begin{rmk}When write a compound statement symbolically, simple statements on the same side of a comma should be grouped together with parentheses.\end{rmk}\pause{}
	\begin{exmp}Let 
		\begin{tabular}{rl}
			$p$: & "Dinner includes soup."\\
			$q$: & "Dinner includes salad."\\
			$r$: & "Dinner includes a side."\\
		\end{tabular}
		
		Write the following in symbolic form.\pause{}
		\begin{enumerate}
			\item Dinner includes soup, and salad or a side.\pause{}\\ $p\wedge (q\vee r)$\pause{}
			\item Dinner includes soup and salad, or a side.\pause{} \\$(p\wedge q)\vee r$
		\end{enumerate}
	\end{exmp}
\end{frame}


\begin{frame}[shrink]{If-Then Statements}
	\begin{deff}
		The compound statement \emph{``If p, then q"} is symbolized by $p\rightarrow q$.  This is called a \emph{conditional statement}.  In this construction, the statement $p$ is the \emph{antecedent}, and the statement $q$ is the \emph{consequent}.
	\end{deff}\pause{}
	\begin{exmp}
		Let $p$ be the statement ``The painting is a pastel" and $q$ be the statement ``The painting is by Beth Anderson."
		
		Write the following symbolically:
		\begin{enumerate}
			\item If the painting is a water color, then it is by Beth Anderson.\pause{} $p\rightarrow q$\pause{}
			\item If the painting is by Beth Anderson, then the painting is not a pastel.\pause{} $q\rightarrow \sim p$
		\end{enumerate}
	\end{exmp}
\end{frame}
\begin{frame}[shrink]{If-Then Statements}
	\begin{exmp}
		Let $p$ and $q$ represent the statements
		\begin{enumerate}
			\item $p$: A person is a father.
			\item $q$: A person is biologically male.
		\end{enumerate}\pause{}
		Write the following statements symbolically:
		\begin{enumerate}
			\item If a person is a father, then that person is biologically male.\pause{} $p\rightarrow q$\pause{}
			\item If a person is not biologically male, then that person is not a father.\pause{} $\sim q \rightarrow \sim p$
		\end{enumerate}
	\end{exmp}
\end{frame}
%\begin{frame}[shrink]{If-Then Statements}
%	\begin{rmk}
%		Given statements $p$ and $q$, the conditional statement $p\rightarrow q$ can be verbalized in many ways:
%		\begin{itemize}
%			\item If $p$ then $q$.
%			\item $q$ if $p$.
%			\item $p$ is sufficient for $q$.
%			\item $q$ is necessary for $p$.
%			\item $p$ only if $q$
%		\end{itemize}
%	\end{rmk}
%\end{frame}

\begin{frame}[shrink]{If and Only If Statements}
	\begin{deff}
		A \emph{biconditional statement} is a conditional statement where $p\rightarrow q$ \textit{and} $q\rightarrow p$
		
		The compound statement ``$p$ if and only if $q$.",\\ abbreviated $p$ iff $q$, \\is denoted $p\leftrightarrow q$.
	\end{deff}\pause{}
	\begin{exmp}
		Let $p$ be the statement ``The painting is a pastel" and $q$ be the statement ``The painting is by Beth Anderson."
		
		Write the following symbolically:
		\begin{enumerate}
			\item The painting is by Beth Anderson if and only if the painting is not a pastel.\pause{} $q\leftrightarrow \sim p$\pause{}
			\item The painting is not by Beth Anderson  if and only if the painting is a pastel.\pause{} $\sim q\leftrightarrow p$
		\end{enumerate}
	\end{exmp}
\end{frame}

\begin{frame}[shrink]{Summary}
	Given statements $p$ and $q$, we can construct any of the following:\\
	\begin{tabular}{|p{.8in}|p{.6in}|l|}
		\hline
		Negation      & $\sim p$                  & Not $p$.                 \\ \hline
		Conjunction   & $p\wedge q$               & $p$ and $q$; $p$ but $q$ \\ \hline
		Disjunction   & $p\vee q$                 & $p$ or $q$ or both       \\ \hline
		Conditional   & $p\rightarrow q$ & If $p$, then $q$         \\ \hline
		Biconditional & $p\leftrightarrow q$      & $p$ if and only if $q$   \\ \hline
	\end{tabular} 
\end{frame}

\begin{frame}[shrink]{Writing Symbolic Statements in Words}
	\begin{exmp}
		Let $p$ and $q$ have the following meaning:
		\begin{enumerate}
			\item $p$: She is wealthy.
			\item $q$: She is happy.
		\end{enumerate}\pause{}
		Write a sentence that describes each of the following:
		\begin{enumerate}
			\item $\sim(p\wedge q)$
			\item $\sim p \wedge q$
			\item $\sim(p\vee q)$
		\end{enumerate}
	\end{exmp}
\end{frame}
%\begin{frame}{Writing Symbolic Statements in Words}
%	\begin{deff}
%		\begin{tabular}{|p{1in}|p{1.1in}|p{1.2in}|}
%			\hline
%			\textbf{Symbolic Statement}          & \textbf{Statements to \newline group together} & \textbf{English\newline Translation}      \\ \hline
%			$(q\wedge \sim p) \rightarrow \sim r$ & $q \wedge \sim p$                      & If $q$ and not $p$, then not $r$. \\ \hline
%			$q\wedge (\sim p \rightarrow \sim r)$ & $\sim p \rightarrow \sim r$                      & $q$, and if not $p$ then not $r$. \\ \hline
%		\end{tabular} 
%	\end{deff}
%\end{frame}
%\begin{frame}{Writing Symbolic Statements in Words}
%	\begin{exmp}
%		Let $p$, $q$, and $r$ represent the statements
%		\begin{itemize}
%			\item $p$: A student misses lecture.
%			\item $q$: A student studies.
%			\item $r$: A student fails the course.
%		\end{itemize}\pause{}
%		Write the following symbolic statements in English:
%		\begin{itemize}
%			\item $(q\wedge \sim p) \rightarrow r$
%			\item $q\wedge(\sim p \rightarrow \sim r)$
%		\end{itemize}
%	\end{exmp}
%\end{frame}

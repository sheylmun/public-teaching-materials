\begin{frame}[shrink]{Objectives}
	\begin{itemize}
		\item Recognize characteristics of normal distributions.
		\item Understand and use the $68-95-99.7$ rule.
		\item Find scores at a specified number of standard deviations from the mean.
		\item Convert a data item to a $z-$score.
		\item Understand percentiles and quartiles.
		\item Use and interpret margins of error.
		\item Recognize distributions that are not normal.
	\end{itemize}
\end{frame}
\begin{frame}[shrink] 
	\begin{deff}
		\emph{The normal distribution}, also called the bell curve or the Gaussian distribution, is `bell-shaped' and symmetrical about the line through its center.  \pause{}
		
		With a normal distribution, the mean, median, and mode are all equal and located exactly in the center of the curve.\pause{}
		
		The exact shape of the normal distribution depends on the mean and the standard deviation.  A data set with a larger standard deviation will have a wider bell curve than one with a smaller standard deviation.
	\end{deff}
\end{frame}
\begin{frame}
	\begin{rmk}
		\begin{itemize}
			\item Approximately $68\%$ of data items fall within $1$ standard deviation of the mean (either above or below).
			
			\item Approximately $95\%$ of the data falls within $2$ standard deviation of the mean (either above or below).
			
			\item Approximately $99.7\%$ of the data falls within $3$ standard deviation of the mean (either above or below).
		\end{itemize}
	\end{rmk}
\end{frame}
\begin{frame}
	\begin{exmp}
		Male adult heights in North America are approximately normally distributed with a mean of 70 inches and a standard deviation of 4 inches.  Find the height that is 2 standard deviations above the mean.\pause{}
		
		Use the normal distribution of adult men's heights to find the percentage of men between $66$ and $74$ inches.\pause{}
		
		What percentage of men are between $62$ and $78$ inches tall?
	\end{exmp}
\end{frame}
\begin{frame}
	\begin{deff}
		A \emph{$z-$score} describes how many standard deviations a data item lies above or below the mean.  The $z-$score of a data item can be found as
		$$z-\text{score} = \dfrac{\text{data item } - \text{ mean}}{\text{standard deviation}}$$
	\end{deff}\pause{}
	\begin{rmk}
		Data items above the mean will have positive $z-$scores and items below will have a negative $z-$score.\pause{} The mean's $z-$score is $0$.
	\end{rmk}
\end{frame}
\begin{frame}
	\begin{exmp}
		The mean weight of newborn infants is 7 pounds and the standard deviation is 0.8 pound.
		
		The weights of newborn infants are normally distributed.  Find the z-score for a weight of 9 
		pounds.
	\end{exmp}\pause{}
	\begin{exmp}
		Intelligence quotients (IQs) on the Stanford–Binet intelligence test are normally distributed with a mean of $100$ and a standard deviation of $16$. What is the IQ corresponding to a z-score of $−1.5$?\pause{}
		
		Mensa members have a $z-$score of $2.05$ or greater among Stanford-Binet test results.  What is the IQ requirement that corresponds with this $z-$score?
	\end{exmp}
\end{frame}
\begin{frame}
	\begin{deff}
		\begin{itemize}
			\item \emph{Percentile}: A data item $x$ is `in the $n^\text{th}$ percentile' if $n\%$ of the data in the set are less than $x$.
			\item \emph{Quartile}: A data item $x$ is `in the $1^\text{st}$ \underline{quartile}' if $25\%$ of data is less than $x$ \pause{}(in other words, if $x$ is in the $25^\text{th}$ percentile).  \pause{} 
			
			A data item $x$ is `in the $2^\text{nd}$ \underline{quartile}' if $50\%$ of data is less than $x$ \pause{}(or, if $x$ is in the $50^\text{th}$ percentile).   \pause{}
			
			A data item $x$ is `in the $3^\text{rd}$ \underline{quartile}' if $75\%$ of data is less than $x$ \pause{}(or, if $x$ is in the $75^\text{th}$ percentile).  
		\end{itemize}
	\end{deff}
\end{frame}
\begin{frame}
	\begin{rmk}
		\begin{itemize}
			\item Using the $z-$score and a table, you can find the percentage of data items that are less than any data item in a normal distribution.\pause{}
			\item In a normal distribution, the mean, median, and mode are the same and therefore have the same $z-$score of 0.  By relating $z-$scores and percentiles, we can say that $50 \% $ of data items are greater than or equal to the mean, median, and mode. \pause{}
			\item Statisticians use properties of the normal distribution to estimate the probability that a result obtained from a single sample reflects what is truly happening. \pause{}
			\item If $n$ is the sample size, there is a $95\%$ probability that a statistic obtained from the random sample will lie within $1/\sqrt{n}$ of the actual population statistic.  This number $1/\sqrt{n}$ is called the \emph{margin of error}.
		\end{itemize}
	\end{rmk}
\end{frame}
\begin{frame}
	\begin{exmp}
		In a random sample of $1172$ children between the ages of $6-$ and $14-$years of age, $17\%$ said getting bossed around is a bad thing about being a kid.
		\begin{enumerate}
			\item Verify the margin of error of the survey.\pause{}
			\item Write a statement about the percentage of all children who feel that getting bossed around is a bad thing. 
		\end{enumerate}
	\end{exmp}
\end{frame}
\begin{frame}{Other kinds of distributions}
	\begin{multicols}{2}
		This graph represents the population distribution of weekly earnings in the United States.  There is no upper limit on weekly earnings.  
		\begin{center}
			\includegraphics[width=0.49\textwidth]{images/SkewedGraph.jpg}
		\end{center}
	\end{multicols}\pause{}
	\begin{itemize}
		\item The relatively few people with very high weekly incomes pull the mean income up higher than the median.  \pause{}
		\item The most frequent income, the mode, occurs toward the low end of the pay spectrum. \pause{}
		\item This is called a \emph{skewed distribution} because a large number of data items are piled at one end and a thin `tail' of data values lies at the other end.\pause{} 
		\item This graph is \emph{skewed to the right} because the higher incomes pull the \underline{mean to the right of the median}.
	\end{itemize}
\end{frame}

\begin{frame}[shrink]{Objectives}
	\begin{itemize}
		\item Calculate measures of central tendency of a data set: the mean, median, mode, and midrange.
		\item Calculate the quartiles of a data set.
	\end{itemize}
\end{frame}
\begin{frame}[shrink]
	\begin{deff}
		\emph{Mean} The sum of the data items divided by the number of items counted.
		$$\text{Mean} = \dfrac{\Sigma x}{n}$$
		where $\Sigma x$ represents the sum of all data and $n$ represents the number of items.
	\end{deff}
	\begin{rmk}
		The word ``average'' usually refers to the ``mean.''
	\end{rmk}
\end{frame}
\begin{frame}[shrink]
	\begin{exmp}
		Calculate the \emph{mean} age of the artists presented in the following data:
		\begin{tabular}{|c|p{1.3in}|c|}
			\hline \textbf{Artist}, year & \textbf{Title} & \textbf{Age} \\ 
			\hline \textbf{Stevie Wonder}, 1963 & Fingertips & 13 \\ 
			\hline \textbf{Donny Osmond}, 1971 & Go Away Little Girl & 13 \\ 
			\hline \textbf{Michael Jackson}, 1972 & Ben & 14 \\ 
			\hline \textbf{Laurie London}, 1958 & He's Got the Whole World in His Hands & 14 \\ 
			\hline \textbf{Chris Brown}, 2005 & Run It! & 15 \\ 
			\hline \textbf{Paul Anka}, 1957 & Diana & 16 \\ 
			\hline \textbf{Brian Hyland}, 1960 & Itsy Bitsy Teenie Weenie Yellow Polkadot Bikini & 16 \\ 
			\hline \textbf{Shaun Cassidy}, 1977 & Da Doo Ron Ron & 17 \\ 
			\hline \textbf{Soulja Boy}, 2007 & Crank that Soulja Boy & 17 \\ 
			\hline \textbf{Sean Kingston}, 2007 & Beautiful Girls & 17 \\ 
			\hline 
		\end{tabular} 
	\end{exmp}
\end{frame}
\begin{frame}
	\begin{rmk}
		When many data values occur more than once, we can organize the data using a frequency distribution, then calculate the mean as follows:
		$$\text{Mean} = \bar{x} = \dfrac{\Sigma x f}{n}$$
		where 
		
		$x$ represent each data value
		
		$f$ represents the frequency of that data value
		
		$\Sigma x f$ is the sum of the product of multiplying each data value by its frequency
		
		$n$ represents the total frequency of the distribution.
	\end{rmk}
\end{frame}
\begin{frame}
	\begin{exmp}
		\begin{multicols}{2}
			Recall the growth rate data from the previous chapter:
			
			\begin{center}
				$12, 14, 14, $\\
				$14, 16, 14, $\\
				$14, 17, 13, $\\
				$10, 13, 18, $\\
				$12, 15, 14, $\\
				$15, 15, 14, $\\
				$14, 13, 15, $\\
				$16, 15, 12, $\\
				$13, 16, 11, $\\
				$15, 12, 13$
			\end{center}
			\vfill\columnbreak\pause{}
			Find the mean using its frequency distribution.
			\begin{center}
				\begin{tabular}{|c|c|}
					\hline Age & frequency\\
					\hline 10 &  1\\ 
					\hline 11 &  1\\ 
					\hline 12 &  4\\ 
					\hline 13 &  5\\ 
					\hline 14 &  8\\ 
					\hline 15 &  6\\ 
					\hline 16 &  3\\ 
					\hline 17 &  1\\ 
					\hline 18 &  1\\
					\hline 
				\end{tabular} 
			\end{center}
		\end{multicols}
	\end{exmp}
\end{frame}
\begin{frame}
	\begin{deff}
		\emph{Median} is the data item exactly in the middle of each set of \textit{ordered} data.
	\end{deff}
	\begin{rmk}
		To find the median of a data set:
		\begin{enumerate}
			\item Arrange the data in order from smallest to largest.
			\item If the number of data items is odd, \textit{the middle item is the median}.
			\item If the number of data items is even, \textit{the mean of the two middle items is the median}.
		\end{enumerate}
	\end{rmk}
	\begin{exmp}
		Find the mean and the median of the set $84, 90, 98, 95, 88$,\pause{}
		
		and of the set $61, 44, 18, 92, 54, 63, 72, 49$
	\end{exmp}
\end{frame}
\begin{frame}
	\begin{exmp}
		Find the median for the frequency distribution:
		
		\begin{tabular}{|c|c|}
			\hline Age & frequency\\
			\hline 10 &  1\\ 
			\hline 11 &  1\\ 
			\hline 12 &  4\\ 
			\hline 13 &  5\\ 
			\hline 14 &  8\\ 
			\hline 15 &  6\\ 
			\hline 16 &  3\\ 
			\hline 17 &  1\\ 
			\hline 18 &  1\\
			\hline 
		\end{tabular} 
	\end{exmp}
\end{frame}
\begin{frame}
	\begin{deff}
		\emph{Mode} is the data value that occurs most often in a data set.  If more than one item occurs with the highest frequency, then each item is a mode.  If no items repeat, there is no mode.
	\end{deff}\pause{}
	\begin{exmp}
		Find the mode for the set $7,2,4,7,8,10$.
	\end{exmp}
\end{frame}
\begin{frame}
	\begin{deff}
		\emph{Midrange} is found by adding the lowest and highest data items, then dividing the result by $2$.
		$$\text{Midrange} = \dfrac{\text{Lowest data value} + \text{Highest data value}}{2}$$
	\end{deff}\pause{}
	\begin{exmp}
		In 2009, the New York Yankees had the greatest payroll, a record \$201,449,181. The Florida Marlins were the worst paid team, with a payroll of \$36,834,000. Find the midrange for the annual payroll of major league baseball teams in 2009.
	\end{exmp}
\end{frame}
\begin{frame}[shrink]
	\begin{deff}
		\emph{Measures of position} are used to make comparisons of a piece of data to the whole population.
		
		Two measures of position are \emph{percentiles} and \emph{quartiles}.  Quartiles divide any data set into $4$ equal parts.  The first quartile is the value that is greater than $25\%$ of the data.  The second quartile is the value that is greater than $50\%$ of the data.  The third quartile is the value that is greater than $75\%$ of the data.
	\end{deff}
	\begin{rmk}
		To determine the quartiles of a data set:
		\begin{enumerate}
			\item Order the data from smallest to larges.\item Find the median (second quartile).\item The first quartile will be the median of the ``lower half'' of the data (all data less than the median).  
			\item Similarly, the third quartile will be the ``upper half'' of the data (all data upwards of the median).
		\end{enumerate}
	\end{rmk}
\end{frame}
\begin{frame}[shrink]
	\begin{rmk}
		Percentiles divide a data set into $100$ equal parts.  If you score in the $85$th percentile on a test, it means you performed better than $85\%$ of people who took the test. (It does not mean that you scored $85\%$ on the test.)
	\end{rmk}
\end{frame}
\begin{frame}[shrink]
	\begin{exmp}
		Find the quartiles of the following data:
		$$18, 8, 13, 16, 3, 22,$$
		$$7, 5, 14, 12, 3, 10, $$ 
		$$10, 23, 23, 22, 13, 5,$$
		$$5, 22, 16, 22, 25, 24, $$
		$$25, 10, 16, 17, 25, 21$$
	\end{exmp}
\end{frame}